from kivy.garden.matplotlib.backend_kivyagg import FigureCanvasKivyAgg
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
import requests
import json
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np
import threading
import requests
import json
from power_algorithm import PowerManager
from api_helper import set_allocations, get_measurement_stream, get_weather_stream
import sys

from kivy.garden.graph import Graph, MeshLinePlot, LinePlot
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.clock import Clock, mainthread
import random

class Real_Time_Plotter_App(App):
    def build(self):
        # ...
        self.lock = threading.Lock()
        self.new_data = False

        self.api_consumer = threading.Thread(target=self.consume_api).start()

        self.plot_len = 400        
        # UI
        self.box = BoxLayout(orientation='vertical')
        self.graph = Graph(xlabel='X', ylabel='Y', x_ticks_minor=5,
                           x_ticks_major=25, y_ticks_major=1,
                           y_grid_label=True, x_grid_label=True, padding=5,
                           xlog=False, ylog=False, x_grid=True, y_grid=True,
                           xmin=-0, xmax=self.plot_len, ymin=-1, ymax=1)
        
        self.charge_graph = Graph(xlabel='X', ylabel='Y', x_ticks_minor=5,
                           x_ticks_major=25, y_ticks_major=10,
                           y_grid_label=True, x_grid_label=True, padding=5,
                           xlog=False, ylog=False, x_grid=True, y_grid=True,
                           xmin=-0, xmax=self.plot_len, ymin=0, ymax=260)
        
        # Different plots
        self.plots = {
            'consumption': LinePlot(color=[1, 0, 0, 1], line_width=4),
            'production': LinePlot(color=[0, 1, 0, 1], line_width=3),
            'storage': LinePlot(color=[0, 0, 1, 1], line_width=4),
            'grid': LinePlot(color=[1, 1, 0, 1], line_width=2),
        }

        self.charge_plot = LinePlot(color=[1, 0, 1, 1], line_width=2)
        
        for plot in self.plots.values():
            plot.points = [(x, 0) for x in range(0, self.plot_len + 1)]
            self.graph.add_plot(plot)

        self.charge_plot.points = [(x, 0) for x in range(0, self.plot_len + 1)]
        self.charge_graph.add_plot(self.charge_plot)

        self.box.add_widget(self.graph)
        self.box.add_widget(self.charge_graph)

        self.label_box = BoxLayout(size_hint=(1, 0.1))
        
        self.label_box.add_widget(Label(text='[color=ff0000]Consumption[/color]', markup=True))
        self.label_box.add_widget(Label(text='[color=00ff00]Production[/color]', markup=True))
        self.label_box.add_widget(Label(text='[color=0000ff]Storage Delivery[/color]', markup=True))
        self.label_box.add_widget(Label(text='[color=ff00ff]Storage Level[/color]', markup=True))
        self.label_box.add_widget(Label(text='[color=ffff00]Grid Usage[/color]', markup=True))
        
        self.box.add_widget(self.label_box)

        Clock.schedule_interval(self.update_plot, 1/10.)
        return self.box

    @mainthread
    def update_plot(self, dt):
        with self.lock:
            if self.new_data:
                
                # Add a new point at the end
                last_x = self.plots['grid'].points[-1][0]
                new_x = last_x + 1

                # Update the y values for each plot
                for plot_name, plot in self.plots.items():
                    plot.points.pop(0)
                    new_y = self.new_data_vals[plot_name]
                    plot.points.append((new_x, new_y))

                # Compute global ymin and ymax across all graphs
                ymin = min([min(plot.points, key=lambda p: p[1])[1] for plot in self.plots.values()])
                ymax = max([max(plot.points, key=lambda p: p[1])[1] for plot in self.plots.values()])

                # Remove all plots
                for plot in self.plots.values():
                    self.graph.remove_plot(plot)
                
                # Update the y range to show the updated data
                self.graph.ymin = float(ymin)
                self.graph.ymax = float(ymax)
                self.graph.xmin = new_x - self.plot_len
                self.graph.xmax = new_x
                self.graph.y_ticks = [(0, '0'), (5, '5'), (10, '10'), (15, '15')]

                # Re-add all plots
                for plot in self.plots.values():
                    self.graph.add_plot(plot)
                
                # Update charge plot
                self.charge_plot.points.pop(0)
                new_y = self.new_data_vals['storage_charge']
                self.charge_plot.points.append((new_x, new_y))
                self.charge_graph.y_ticks = [(0, '0'), (50, '50'), (100, '100'), (150, '150'), (200, '200'), (250, '250')]
                # self.charge_graph.ymin = float(np.min([p[1] for p in self.charge_plot.points]))
                # self.charge_graph.ymax = float(np.max([p[1] for p in self.charge_plot.points]))
                self.charge_graph.remove_plot(self.charge_plot)
                self.charge_graph.add_plot(self.charge_plot)
                

                self.charge_graph.xmin = new_x - self.plot_len
                self.charge_graph.xmax = new_x
                

                self.new_data = False
                # print(f"Updated plot with new data")
    
    def consume_api(self):
        print("Consuming API")
        lines = get_measurement_stream()
        for line in lines:
            with self.lock:
                
                json_str = line.decode('utf-8')
                data = json.loads(json_str)

                self.new_data_vals = {
                    'consumption': data['consumer']['consumption']['value'],
                    'production': data['producer']['to_storage']['value'] \
                            + data['producer']['to_consumption']['value'] \
                            + data['producer']['to_grid']['value'],
                    'storage': data['storage']['to_consumption']['value'],
                    'storage_charge': data['storage']['charge']['value'],
                    'grid': data['grid']['to_consumption']['value'],
                }

                self.new_data = True

                # print(f"New data: {self.new_data_vals}")

Real_Time_Plotter_App().run()
