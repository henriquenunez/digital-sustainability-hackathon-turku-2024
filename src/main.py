import threading
import requests
import json
from power_algorithm import PowerManager
from api_helper import set_allocations, get_measurement_stream, get_weather_stream
import sys

# Shared state variable
shared_state = 0

# Lock for synchronization
lock = threading.Lock()

power_manager = PowerManager()

def control_thread():
    global shared_state
    
    lines = get_measurement_stream()
    for line in lines:
        
        with lock:
            print("Reading shared state:", shared_state)
        
        # print(line)

        json_str = line.decode('utf-8')
        data = json.loads(json_str)

        allocation = power_manager.compute_allocation(data)
        set_allocations(allocation)
        # Set allocations
    
def weather_thread():
    global shared_state

    lines = get_weather_stream()
    
    for line in lines:
        #parse line as JSON and do whatever
        with lock:
            shared_state += 1
            # print("Writing to shared state:", shared_state)

        # print(line)

def main():
    # Create and start the threads
    control = threading.Thread(target=control_thread)
    weather = threading.Thread(target=weather_thread)
    control.start()
    weather.start()

main()
