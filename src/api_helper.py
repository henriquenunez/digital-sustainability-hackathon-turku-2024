# Functions related to allocation

import requests
import json
import dataclasses
import os

@dataclasses.dataclass
class Allocation:
    to_storage: float
    to_consumption: float
    to_grid: float

BASE_URL = "https://hackathon.kvanttori.fi"

"""
Grid:
{
  "to_storage": 0
}
Production:
{
  "to_consumption": 0,
  "to_grid": 0,
  "to_storage": 0
}
Storage:
{
  "to_consumption": 0,
  "to_grid": 0
}
"""
def set_allocations(setup, id='da2b58e8-b8a6-493b-93cf-e2e342eae626'):
    if 'BUILDING_ID' in os.environ:
      id = os.environ['BUILDING_ID']

    base_url = BASE_URL + '/buildings/' + id + '/allocations'

    if 'grid' in setup.keys():
        url = base_url + '/grid_allocation'
        response = requests.post(url, json=setup['grid'])
        print(f'grid -> {response}')

    if 'production' in setup.keys():
        url = base_url + '/production_allocation'
        response = requests.post(url, json=setup['production'])
        print(f'production -> {response}')

    if 'storage' in setup.keys():
        url = base_url + '/storage_allocation'
        response = requests.post(url, json=setup['storage'])
        print(f'storage -> {response}')

def get_measurement_stream(id='da2b58e8-b8a6-493b-93cf-e2e342eae626'):
    if 'BUILDING_ID' in os.environ:
      id = os.environ['BUILDING_ID']

    return requests.get(BASE_URL + '/buildings/' + id + '/streams', stream=True).iter_lines()

def get_weather_stream():
    if 'BUILDING_ID' in os.environ:
      id = os.environ['BUILDING_ID']

    return requests.get(BASE_URL + '/system/stream', stream=True).iter_lines()
