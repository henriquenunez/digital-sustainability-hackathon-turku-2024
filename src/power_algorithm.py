
# {'producer': {'to_storage': {'timestamp': '2024-06-27T13:59:42+03:00', 'value': 4.946089013024194, 'unit': 'kWh'}, 'to_consumption': {'timestamp': '2024-06-27T13:59:42+03:00', 'value': 0.0, 'unit': 'kWh'}, 'to_grid': {'timestamp': '2024-06-27T13:59:42+03:00', 'value': 0.0, 'unit': 'kWh'}}, 'consumer': {'consumption': {'timestamp': '2024-06-27T13:59:42+03:00', 'value': 6.148266003470945, 'unit': 'kWh'}}, 'storage': {'to_consumption': {'timestamp': '2024-06-27T13:59:42+03:00', 'value': 2.70164858944345, 'unit': 'kWh'}, 'to_grid': {'timestamp': '2024-06-27T13:59:42+03:00', 'value': 0.0, 'unit': 'kWh'}, 'charge': {'timestamp': '2024-06-27T13:59:42+03:00', 'value': 4.946089013024194, 'unit': 'kWh'}}, 'grid': {'to_storage': {'timestamp': '2024-06-27T13:59:42+03:00', 'value': 0.0, 'unit': 'kWh'}, 'to_consumption': {'timestamp': '2024-06-27T13:59:42+03:00', 'value': 3.4466174140274948, 'unit': 'kWh'}}}
import numpy as np

class PowerManager:
    def __init__(self) -> None:
        self.total_production_history = []
        self.consumption_history = []
        self.grid_usage_history = []
        self.rolling_avg_window = 1

    def add_average_history(self, consumption, total_production, grid_usage):
        self.total_production_history.append(total_production)
        self.consumption_history.append(consumption)
        self.grid_usage_history.append(grid_usage)

        if len(self.total_production_history) > self.rolling_avg_window:
            self.total_production_history.pop(0)
            self.consumption_history.pop(0)
            self.grid_usage_history.pop(0)

        return np.mean(self.consumption_history), np.mean(self.total_production_history), np.mean(self.grid_usage_history)

    def compute_allocation(self, data):
        # print('Compute allocation: ' + str(data))
        consumption = data['consumer']['consumption']['value']
        total_production = data['producer']['to_storage']['value'] \
                            + data['producer']['to_consumption']['value'] \
                            + data['producer']['to_grid']['value']
        storage_level = data['storage']['charge']['value']
        grid_usage = data['grid']['to_consumption']['value'] + data['grid']['to_storage']['value']

        consumption, total_production, grid_usage = self.add_average_history(consumption, total_production, grid_usage)

        is_peak_consumption = True if consumption > 8 else False
        is_storage_empty = True if storage_level <= 0.01 else False
        is_low_production = True if total_production < 3 else False
        is_low_consumption = True if consumption < 6 else False
        is_low_grid = True if grid_usage < 6 else False

        cons_prod_ratio = round(consumption / total_production, 1)

        """
        if is_peak_consumption:
            offset = consumption - total_production
            
            if not is_storage_empty:
                if offset < storage_level:
                    storage_delivery = offset / storage_level
                else:
                    storage_delivery = 1.0
            else:
                storage_delivery = 0

            allocation = {
                'production' : {
                    'to_consumption' : 1.0,
                    'to_grid' : 0,
                    'to_storage' : 0
                },
                'grid' : {
                    'to_storage': 0
                },
                'storage': {
                    'to_consumption': storage_delivery,
                    'to_grid': 0
                }
            }
        elif cons_prod_ratio < 1:

            allocation = {
                'production' : {
                    'to_consumption' : cons_prod_ratio,
                    'to_grid' : 0,
                    'to_storage' : 1 - cons_prod_ratio
                },
                'grid' : {
                    'to_storage': 0
                },
                'storage': {
                    'to_consumption': 0,
                    'to_grid': 0
                }
            }
        elif is_low_production and is_low_consumption:
            allocation = {
                'production' : {
                    'to_consumption' : 1.0,
                    'to_grid' : 0,
                    'to_storage' : 0
                },
                'grid' : {
                    'to_storage': 1.0
                },
                'storage': {
                    'to_consumption': 0,
                    'to_grid': 0
                }
            }
        elif consumption > 6 and consumption < 12:
            allocation = {
                'production' : {
                    'to_consumption' : 1.0,
                    'to_grid' : 0,
                    'to_storage' : 0
                },
                'grid' : {
                    'to_storage': 1.0
                },
                'storage': {
                    'to_consumption': 0,
                    'to_grid': 0
                }
            }
        else:
            offset = consumption - total_production
            
            if not is_storage_empty:
                if offset > 0.01:
                    storage_delivery = 0
                elif offset < storage_level:
                    storage_delivery = offset / storage_level
                else:
                    storage_delivery = 1.0
            else:
                storage_delivery = 0

            allocation = {
                'production' : {
                    'to_consumption' : 1.0,
                    'to_grid' : 0,
                    'to_storage' : 0
                },
                'grid' : {
                    'to_storage': 0
                },
                'storage': {
                    'to_consumption': storage_delivery,
                    'to_grid': 0
                }
            }
        """

        if storage_level < 200 and is_low_grid:
            grid_allocation = {
                'to_storage': (storage_level + 5) / 250
            }
        else:
            grid_allocation = {
                'to_storage': 0
            }

        # If we are consuming more than we are producing
        if cons_prod_ratio > 1:

            offset = consumption - total_production
            if offset < storage_level:
                storage_delivery = offset / storage_level
            else:
                storage_delivery = 1.0

            # Prioritize consumption
            if is_peak_consumption:

                storage_delivery *= 0.5

                allocation = {
                    'production' : {
                        'to_consumption' : 1.0,
                        'to_grid' : 0,
                        'to_storage' : 0
                    },
                    'storage': {
                        'to_consumption': storage_delivery,
                        'to_grid': 0
                    }
                }
            
            else:
                storage_delivery *= 0.7

                # Remaining consumption is taken from grid
                allocation = {
                    'production' : {
                        'to_consumption' : 1.0,
                        'to_grid' : 0,
                        'to_storage' : 0
                    },
                    'storage': {
                        'to_consumption': 0,
                        'to_grid': 0
                    }
                }

        else:
            # Remaining production is stored
            allocation = {
                'production' : {
                    'to_consumption' : cons_prod_ratio,
                    'to_grid' : 0,
                    'to_storage' : 1 - cons_prod_ratio
                },
                'grid' : {
                    'to_storage': 0
                },
                'storage': {
                    'to_consumption': 0,
                    'to_grid': 0
                }
            }

        allocation['grid'] = grid_allocation

        print(f"Timestamp:{data['consumer']['consumption']['timestamp']}\nConsumption: {consumption}\nStorage Level:{storage_level}\nTotal production: {total_production}\nRatio: {cons_prod_ratio}\nAllocation:{allocation}\n-------------------")
        print(f'Allocation:{allocation}\n-------------------')

        return allocation

"""
Return things this way:

my_setup = {
    'production' : {
        'to_consumption' : 0,
        'to_grid' : 0,
        'to_storage' : 1.0
    },
    'grid' : {
      'to_storage': 0
    },
    'storage': {
      'to_consumption': 1.0,
      'to_grid': 0
    }
}
"""
