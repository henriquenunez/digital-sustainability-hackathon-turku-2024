#!/bin/bash

rsync -avz . plutonium:~/hackathon
# ssh -t zincum 'sudo rm -rf /var/www/portfolio; sudo cp -r ~/portfolio /var/www; sudo chown -R www-data:www-data /var/www/portfolio'
ssh -t plutonium 'cd ~/hackathon; ./run_instance.sh'
