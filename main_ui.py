from kivy.garden.matplotlib.backend_kivyagg import FigureCanvasKivyAgg
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
import requests
import json
import matplotlib.pyplot as plt
from datetime import datetime

"""
{
  "id": "da2b58e8-b8a6-493b-93cf-e2e342eae626",
  "team_name": "vesisavua",
  "data": {
    "producer": {
      "to_storage": [],
      "to_consumption": [],
      "to_grid": []
    },
    "consumer": {
      "consumption": []
    },
    "storage": {
      "capacity": 250.0,
      "charge": [
        {
          "timestamp": "2024-05-03T17:10:10.196356077+03:00",
          "value": 0.0,
          "unit": "kW"
        }
      ],
      "to_consumption": [],
      "to_grid": []
    },
    "grid": {
      "to_storage": [],
      "to_consumption": []
    }
  },
  "building_params": {
    "stor_to_con_alloc": 0.0,
    "stor_to_grid_alloc": 0.0,
    "prod_to_con_alloc": 0.0,
    "prod_to_stor_alloc": 0.0,
    "prod_to_grid_alloc": 1.0,
    "grid_to_stor_alloc": 0.0
  }
}
"""

plt.plot([1, 23, 2, 4])
plt.ylabel('some numbers')

BASE_URL = "https://hackathon.kvanttori.fi"

def get_data(url):
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        return data
    else:
        print("Error: Failed to retrieve data")
        return None

def get_system_state():
    response = requests.get(BASE_URL + '/system')
    if response.status_code == 200:
        data = response.json()
        data['dt_timestamp'] = datetime.strptime(data['time'], "%Y-%m-%dT%H:%M:%S%z")

        return data
    else:
        # TODO: raise exception
        print("Error: Failed to retrieve data")
        return None

def get_1_day_measurements():
    pass

def post_data(url, data):
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url, json=data, headers=headers)
    if response.status_code == 200:
        print("Data posted successfully")
    else:
        print("Error: Failed to post data")

class MyApp(App):

    def build(self):
        box = BoxLayout()
        box.add_widget(FigureCanvasKivyAgg(plt.gcf()))
        return box

# print(get_system_state())

MyApp().run()