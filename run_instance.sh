#!/bin/bash

# Check if the Python virtual environment exists
if [ ! -d "venv" ]; then
    # Create a new Python virtual environment
    python3 -m venv venv
fi

# pwd
screen -wipe

# Kill all screens matching "my_screen"
screen -ls | awk '/my_screen/ {print $1}' | grep -o '[0-9]*' | while read -r screen_id; do
    screen -S "$screen_id" -X quit
done

# Create a new screen session
screen -dmS my_screen

# Run your program inside the screen session
screen -S my_screen -X stuff "source venv/bin/activate;pip install -r requirements.txt;BUILDING_ID=19384603-839f-4e5d-b729-509e06809676 python3 src/main.py$(printf \\r)"

# Detach the screen session
screen -S my_screen -X detach
