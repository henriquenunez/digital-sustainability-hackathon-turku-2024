# Digital Sustainability Hackathon 2024

Event took place in Turku, FI

## Challenge description

This challenge is about using energy metrics from a simulated building to allow using energy more efficiently. More and more buildings have some kind of energy monitoring systems but they might not work together or data from them might not be used to actually improve energy consumption. How could the data be used to actually reduce energy consumption and by extension energy costs for tenants of buildings?

We were given an API that allows allocating and reading consumption usage from a simulated building. A scheme of the diferent elements can be seen:

![Allocation schemes](assets/allocations.jpg)

## Technologies and implementation

Our solution consists of 2 applications. One "daemon" application that consumes the API and dynamically allocates the grid, storage and production outputs. For monitoring, we developed a GUI application to plot in real time the production and consumption as well as the status of the storage. We visually found that our system made the simulated environment less reliant on the grid. 

## Instructions

- Have python >= 3.10 installed
- Install the `requirements.txt`

### Server

- For the server, run `python src/main.py`

### UI

- For the GUI run `python src/ui.py`

Using a default "Building ID" generated during the competition. To pass a custom building ID, set an environment variable before running, like `BUILDING_ID=your_building_id python src/main.py`

## Screenshots

![GUI Sample (control)](assets/Screenshot%202024-05-04%20at%2018.44.28.png)
![GUI Sample (dynamic allocations)](assets/photo_2024-05-04_23-23-39.jpg)
